<?php


namespace calderawp\InteropCore;

use calderawp\interop\Exceptions\Exception;
use Symfony\Component\Validator\Validator\ValidatorInterface as Validator;
use Symfony\Component\Validator\Constraint;

interface Attribute
{
    /**
     * @return bool
     */
    public function isValid();

    /**
     * @return mixed
     * @throws Exception
     */
    public function getValue();

    /**
     * Get identifier for the attribute
     *
     * @return string
     */
    public function getIdentifier();

    /**
     * @return Constraint[]
     */
    public function getConstraints();

    /** @return string */
    public function getPrimitive();
}
