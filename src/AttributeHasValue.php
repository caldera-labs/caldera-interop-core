<?php


namespace calderawp\InteropCore;

/**
 * Trait AttributeHasValue
 * @package calderawp\InteropCore
 */
trait AttributeHasValue
{
    /** @var  mixed */
    private $value;

    /**
     * Get value with no validation
     *
     * @return mixed
     */
    protected function getRawValue()
    {
        return $this->value;
    }

    /**
     * Set value with no validation
     *
     * @param mixed $raw
     */
    protected function setRawValue($raw)
    {
        $this->value = $raw;
    }
}
