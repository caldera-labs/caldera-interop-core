<?php


namespace calderawp\InteropCore;

use Throwable;

class Exception extends \calderawp\interop\Exceptions\Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
