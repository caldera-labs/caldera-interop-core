<?php


namespace calderawp\InteropCore;

trait HasValidatingAttributes
{
    /**
     * @var array
     */
    protected $attributes;

    /**
     * @param string $identifier
     * @return bool
     */
    public function hasAttribute($identifier)
    {
        return !empty($this->attributes[$identifier]);
    }

    /**
     * @param string $identifier
     * @return bool
     */
    public function allowedAttribute($identifier)
    {
        return array_key_exists($identifier, $this->attributes);
    }

    /**
     * Get attribute
     *
     * @param string $identifier
     * @return ValidatingAttribute
     * @throws Exception
     */
    public function getAttribute($identifier)
    {
        if (!$this->allowedAttribute($identifier)) {
            throw new Exception();
        }

        if (!$this->hasAttribute($identifier)) {
            throw new Exception();
        }

        return $this->attributes[$identifier];
    }

    /**
     * Get all attributes
     *
     * @return array
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @return array
     */
    public function getAttributeNames()
    {
        return array_keys($this->attributes);
    }
}
