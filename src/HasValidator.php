<?php


namespace calderawp\InteropCore;

use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

trait HasValidator
{

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * Get validator instance
     *
     * @return ValidatorInterface
     */
    protected function getValidator()
    {
        if (!$this->validator) {
            $this->validator = Validation::createValidator();
        }

        return $this->validator;
    }

    /**
     * (re)Set validator
     *
     * @param ValidatorInterface $validator
     */
    protected function setValidator(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }
}
