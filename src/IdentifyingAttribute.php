<?php


namespace calderawp\InteropCore;

trait IdentifyingAttribute
{

    /**
     * Get identifier for the attribute
     *
     * @return string
     */
    public function getIdentifier()
    {
        return substr(strrchr(get_class($this), '\\'), 1);
    }
}
