<?php


namespace calderawp\InteropCore;

use calderawp\interop\Interfaces\JsonArrayable;
use Psr\Http\Message\RequestInterface as Request;

abstract class InteroperableModel implements Interoperable, JsonArrayable
{
    use RegistersAttributes;

    /**
     * @var array
     */
    private $defaults;

    /**
     * @var array
     */
    private $requiredAttributes;

    /**
     * @var array
     */
    private $attributeValidates;

    /**
     * InteroperableModel constructor.
     * @param array [ string => array ] $defaultValues Array of default values
     */
    public function __construct(array $defaultValues = [])
    {
        $this->setupAttributes();

        $this->setDefaultValues($defaultValues);
    }


    /** @inheritdoc */
    public static function fromRequest(Request $request)
    {
        $obj = new static();
        $body = json_decode($request->getBody()->getContents());

        /** @var Attribute $attribute */
        foreach ($obj->getAttributes() as $attribute) {
            $attributeType = $attribute->getIdentifier();
            if (isset($body->$attributeType)) {
                $obj->setAttributeValue(
                    $attributeType,
                    $body->$attributeType
                );
            } else {
                $obj->setAttributeValue(
                    $attributeType,
                    $obj->getDefault($attributeType)
                );
            }
        }

        return $obj;
    }

    /**
     * Validate all attributes
     */
    protected function validateAll()
    {
        $this->attributeValidates = [];
        /** @var Attribute $attribute */
        foreach ($this->getAttributes() as $attribute) {
            $this->attributeValidates[$attribute->getIdentifier()] = $attribute->isValid();
        }
    }

    /** @inheritdoc */
    public function isValid()
    {
        if (empty($this->attributeValidates)) {
            $this->validateAll();
        }

        foreach ($this->attributeValidates as $attributeIdentifier => $isValid) {
            if (! $isValid) {
                return false;
            }
        }

        return true;
    }

    /**
     * Get an array of invalid attributes
     *
     * @return array
     */
    public function getInvalids()
    {
        $invalids = [];
        if (empty($this->attributeValidates)) {
            $this->validateAll();
        }

        foreach ($this->attributeValidates as $attributeIdentifier => $isValid) {
            if (! $isValid) {
                $invalids[] = $attributeIdentifier;
            }
        }

        return $invalids;
    }

    /** @inheritdoc */
    public function toResponseBody()
    {
        return json_encode($this);
    }

    /** @inheritdoc */
    public function toArray()
    {
        $array = [];
        /** @var Attribute $attribute */
        foreach ($this->getAttributes() as $attribute) {
            $array[$attribute->getIdentifier()] = $attribute->getValue();
        }
        return $array;
    }

    /** @inheritdoc */
    public function jsonSerialize()
    {
        return $this->toArray();
    }

    /**
     * @param $identifier
     * @return mixed
     * @throws Exception
     */
    public function getAttributeValue($identifier)
    {
        try {
            $attribute = $this->getAttribute($identifier);
        } catch (Exception $e) {
            throw $e;
        }

        try {
            $value = $attribute->getValue();

            if (!$value) {
                if ($this->hasDefault($identifier)) {
                    return $this->getDefault($identifier);
                }
            }
            return $value;
        } catch (Exception $e) {
            if ($this->hasDefault($identifier)) {
                return $this->getDefault($identifier);
            }
            throw $e;
        }
    }

    /**
     * Check if attribute has default for the model
     *
     * @param string $identifier
     * @return bool
     */
    public function hasDefault($identifier)
    {
        return array_key_exists($identifier, $this->defaults);
    }

    /**
     * @param string $identifier
     * @return mixed
     */
    public function getDefault($identifier)
    {
        if ($this->hasAttribute($identifier) && $this->hasDefault($identifier)) {
            return $this->defaults[$identifier];
        }
    }

    /**
     * Set attribute value
     *
     * @param string $type Attribute type -- must match Attribute::getIdentifier();
     * @param mixed $value Value to set. Must be a valid value for this attribute
     * @throws Exception
     */
    protected function setAttributeValue($type, $value)
    {
        if ($this->hasAttribute($type)) {
            try {
                $this->getAttribute($type)->setValue($value);
            } catch (Exception $e) {
                throw new Exception($type . $e->getMessage());
            }
        }
    }

    /**
     * Define attributes a concrete model has
     *
     * @return Attribute[]
     */
    abstract protected function defineAttributes();

    /**
     * @param array $defaultValues
     */
    protected function setDefaultValues(array $defaultValues)
    {
        foreach ($defaultValues as $type => $value) {
            if (empty($this->getAttribute($type)->getValue())) {
                $this->setAttributeValue($type, $value);
            }
        }
    }

    /**
     * Setup model attributes
     */
    protected function setupAttributes()
    {
        foreach ($this->defineAttributes() as $attributeDefinition) {
            $attributeDefinition = (object)$attributeDefinition;
            $this->registerAttribute($attributeDefinition->attribute);
            if (!empty($attributeDefinition->default)) {
                $this->defaults[$attributeDefinition->attribute->getIdentifier()] = $attributeDefinition->default;
            }
            if (isset($attributeDefinition->required)) {
                $this->requiredAttributes[] = $attributeDefinition->attribute->getIdentifier();
            }
        }
    }
}
