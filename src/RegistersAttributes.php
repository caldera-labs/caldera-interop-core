<?php


namespace calderawp\InteropCore;

trait RegistersAttributes
{
    use HasValidatingAttributes;

    /**
     * Register an attribute
     *
     * @param Attribute $attribute
     */
    protected function registerAttribute(Attribute $attribute)
    {
        $this->attributes[ $attribute->getIdentifier() ] = $attribute;
    }
}
