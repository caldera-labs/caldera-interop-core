<?php


namespace calderawp\InteropCore;

use \calderawp\InteropCore\Exception;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

abstract class ValidatingAttribute implements Attribute
{
    use AttributeHasValue, HasValidator, IdentifyingAttribute;

    /** @var  bool */
    protected $validationRan;
    /** @var  bool|null */
    protected $result;

    /* @inheritdoc */
    public function isValid()
    {
        if (!$this->validationRan) {
            $this->validate();
        }

        return $this->result;
    }

    /**
     * Runs actual validation when needed, setting properties
     */
    protected function validate()
    {
        $this->validationRan = true;
        try {
            $validator = $this->getValidator()->validate(
                $this->getRawValue(),
                $this->getConstraints(),
                null
            );
            if (0 === $validator->count()) {
                $this->result = true;
            } else {
                foreach ($validator as $thing) {
                    $this->result[] = $thing;
                }
            }
        } catch (Exception $e) {
            $this->result = $e->getMessage();
        }
    }

    /**
     * Get attribute value
     *
     * @return mixed
     * @throws \calderawp\InteropCore\Exception
     */
    public function getValue()
    {
        if (!$this->isValid()) {
            if (is_array($this->result)) {
                $this->result = implode(' ', $this->result);
            }
            throw new Exception($this->result);
        }

        return $this->getRawValue();
    }

    /**
     * Set value of attribute
     *
     * @param mixed $value
     *
     * @return  $this
     * @throws \calderawp\InteropCore\Exception
     */
    public function setValue($value)
    {
        $value = $this->castPrimitive($value);
        $this->validationRan = false;
        $this->result = null;
        try {
            $validator = $this->getValidator()
                ->validate(
                    $value,
                    $this->getConstraints()
                );
        } catch (\Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode(), $e);
        }

        if (empty($validator->count())) {
            $this->result = true;
            $this->setRawValue($value);
            return $this;
        }

        throw new \calderawp\InteropCore\Exception();
    }

    /**
     * Casts a value to primitive if supported and needed
     *
     * @param mixed $value Current value
     * @return mixed
     */
    protected function castPrimitive($value)
    {
        switch ($this->getPrimitive()) {
            case \DateTime::class:
                $value = $this->dateCast($value);
                break;
        }
        return $value;
    }

    /**
     * @param $value
     * @return \DateTime|false|int
     */
    protected function dateCast($value)
    {
        if (! is_object($value) || !is_a($value, \DateTime::class)) {
            if (is_string($value)) {
                $value = new \DateTime($value);
            } elseif (is_a($value, '\stdClass') && isset($value->date)) {
                $value = new \DateTime(
                    $value->date,
                    property_exists($value, 'timezone')
                        ? new \DateTimeZone($value->timezone)
                        : null
                );
            } else {
            }
        }
        return $value;
    }
}
