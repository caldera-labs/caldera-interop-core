<?php


namespace calderawp\InteropCore\Tests;

use calderawp\InteropCore\IdentifyingAttribute;
use calderawp\InteropCore\Tests\Mocks\PublishDate;

use Symfony\Component\Validator\Constraints\Date;

class AttributeTest extends TestCase
{

    /**
     * Test getting constraints
     *
     * @covers PublishDate::getConstraints()
     */
    public function testGetConstraints()
    {
        $attribute = new PublishDate();
        $constraints = $attribute->getConstraints();
        $this->assertEquals(
            Date::class,
            get_class($constraints[0])
        );
    }

    /**
     * Test getting identifier
     *
     * @covers IdentifyingAttribute::getIdentifier()
     */
    public function testGetIdentifier()
    {
        $attribute = new PublishDate();

        $this->assertEquals(
            'PublishDate',
            $attribute->getIdentifier()
        );
    }
}
