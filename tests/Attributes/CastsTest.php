<?php


namespace calderawp\InteropCore\Tests\Attributes;

use calderawp\InteropCore\Tests\Mocks\PublishDate;
use calderawp\InteropCore\Tests\TestCase;

class CastsTest extends TestCase
{
    /**
     * Test that attribute casts date from string properly
     *
     * @covers ValidatingAttribute::dateCast()
     * @covers ValidatingAttribute::castPrimitive()
     */
    public function testDateCastFromString()
    {
        $date = new \DateTime('2000-01-01');
        $dateString = $date->format('Y-m-d');
        $attribute = new PublishDate();
        $attribute->setValue($dateString);

        $this->assertInstanceOf(
            \DateTime::class,
            $attribute->getValue()
        );


        $this->assertEquals(
            new \DateTime('2000-01-01'),
            $attribute->getValue()
        );
    }

    /**
      * Test that attribute casts date from stdClass with property date set properly
     *
     *
     * @covers ValidatingAttribute::dateCast()
     * @covers ValidatingAttribute::castPrimitive()
     */
    public function testCastFromStdClass()
    {
        $obj = new \stdClass();
        $obj->date = '2000-01-01';
        $attribute = new PublishDate();
        $attribute->setValue($obj);

        $this->assertSame($obj->date, $attribute->getValue()->format('Y-m-d'));
    }

    /**
     * Test that attribute can be set using \DateTime() properly
     *
     * @covers ValidatingAttribute::dateCast()
     * @covers ValidatingAttribute::castPrimitive()
     */
    public function testCastFromDateTime()
    {
        $date = new \DateTime('2000-01-01');
        $attribute = new PublishDate();
        $attribute->setValue($date);
        $this->assertSame($date->format('Y-m-d'), $attribute->getValue()->format('Y-m-d'));
    }
}
