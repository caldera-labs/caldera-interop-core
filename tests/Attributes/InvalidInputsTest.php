<?php


namespace calderawp\InteropCore\Tests\Attributes;

use calderawp\InteropCore\Tests\Mocks\AuthorName;
use calderawp\InteropCore\Tests\Mocks\PublishDate;
use calderawp\InteropCore\Tests\Mocks\Title;
use calderawp\InteropCore\Tests\TestCase;

class InvalidInputsTest extends TestCase
{

    /**
     * Test invalid value is not considered valid an attribute we have a primate caster for
     *
     * @covers ValidatingAttribute::setValue()
     * @covers ValidatingAttribute::isValid()
     * @covers AttributeHasValue::setRawValue()
     */
    public function testValueInvalid()
    {
        $attribute = new PublishDate();
        $this->expectDefaultException();
        $attribute->setValue([]);
    }

    /**
     * Test invalid value is not considered valid we do not have we have a primate caster for.
     *
     * @covers ValidatingAttribute::setValue()
     * @covers ValidatingAttribute::isValid()
     * @covers AttributeHasValue::setRawValue()
     */
    public function testConstantStringTooLong()
    {
        $attribute = new AuthorName();
        $this->expectDefaultException();
        $attribute->setValue($this->randomString(88));
    }

    /**
     * Test that setting invalid value (too long string) on an attribute with multiple constraints raises exceptions
     *
     * @covers ValidatingAttribute::setValue()
     * @covers ValidatingAttribute::getValue()
     */
    public function testMultipleConstraintsInValidTooLongString()
    {
        $attribute = new Title();
        $this->expectDefaultException();

        $attribute->setValue($this->randomString(433));
    }

    /**
     * Test that setting invalid value (null) on an attribute with multiple constraints raises exceptions
     *
     * @covers ValidatingAttribute::setValue()
     * @covers ValidatingAttribute::getValue()
     */
    public function testMultipleConstraintsInValidNull()
    {
        $attribute = new Title();
        $this->expectDefaultException();
        $attribute->setValue(null);
    }

    /**
     * Test that setting invalid value (null) on an attribute with multiple constraints raises exceptions
     *
     * @covers ValidatingAttribute::setValue()
     * @covers ValidatingAttribute::getValue()
     * @covers HasValidator::getValidator()
     */
    public function testMultipleConstraintsEmptyString()
    {
        $attribute = new Title();
        $this->expectDefaultException();
        $attribute->setValue('');
    }

    /**
     * Test that setting invalid value (not string) on an attribute with multiple constraints raises exceptions
     *
     * @covers ValidatingAttribute::setValue()
     * @covers ValidatingAttribute::getValue()
     * @covers HasValidator::getValidator()
     */
    public function testMultipleConstraintsInValidNotString()
    {
        $attribute = new Title();
        $this->expectDefaultException();
        $attribute->setValue(new \DateTime());
    }
}
