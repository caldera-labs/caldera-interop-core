<?php


namespace calderawp\InteropCore\Tests\Attributes;

use calderawp\InteropCore\Tests\Mocks\PublishDate;
use calderawp\InteropCore\Tests\Mocks\Title;
use calderawp\InteropCore\Tests\TestCase;

class ValidInputsTest extends TestCase
{

    /**
     * Test valid value is considered valid
     *
     * @covers ValidatingAttribute::isValid()
     * @covers ValidatingAttribute::setValue()
     * @covers AttributeHasValue::setRawValue()
     */
    public function testValueValid()
    {
        $attribute = new PublishDate();
        $value = new \DateTime();
        $attribute->setValue($value);
        $this->assertTrue($attribute->isValid());
    }

    /**
     * Test getting value
     *
     * @covers ValidatingAttribute::getValue()
     * @covers PublishDate::setValue()
     * @covers AttributeHasValue::setRawValue()
     * @covers AttributeHasValue::getRawValue()
     */
    public function testGetValue()
    {
        $attribute = new PublishDate();
        $value = new \DateTime();
        $attribute->setValue($value);
        $this->assertSame($attribute->getValue(), $value);
    }


    /**
     * Test setting a value that is valid on attribute with multiple constraints
     *
     * @covers ValidatingAttribute::setValue()
     */
    public function testMultipleConstraintsValid()
    {
        $attribute = new Title();
        $attribute->setValue($this->randomString(42));
        $this->assertTrue($attribute->isValid());

        $attribute->setValue($this->randomString(11));
        $this->assertTrue($attribute->isValid());
    }
}
