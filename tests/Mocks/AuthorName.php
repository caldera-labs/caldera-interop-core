<?php


namespace calderawp\InteropCore\Tests\Mocks;

use calderawp\InteropCore\IdentifyingAttribute;
use calderawp\InteropCore\ValidatingAttribute;
use Symfony\Component\Validator\Constraints\Isbn;
use Symfony\Component\Validator\Constraints\Length;

class AuthorName extends ValidatingAttribute
{
    use IdentifyingAttribute;

    /** @inheritdoc */
    public function getConstraints()
    {
        $length = new Length();
        $length->min = 1;
        $length->max = 42;
        return [
            $length
        ];
    }

    /** @inheritdoc */
    public function getPrimitive()
    {
        return 'string';
    }
}
