<?php


namespace calderawp\InteropCore\Tests\Mocks;

use calderawp\InteropCore\InteroperableModel;
use Psr\Http\Message\ResponseInterface as Response;

class BookModel extends InteroperableModel
{
    const TITLE_DEFUALT = 'Insert Title';

    /** @inheritdoc */
    protected function defineAttributes()
    {
        return [
            [
                'attribute' => new PublishDate(),
                'required' => true,
            ],
            [
                'attribute' => new Title(),
                'required' => true,
                'default' => self::TITLE_DEFUALT,
            ]


        ];
    }


    /** @inheritdoc */
    public function toResponse()
    {
        if ($this->isValid()) {
            return new BookResponse(
                200,
                [
                    'Content-Type' => 'application/json'
                ],
                $this->toResponseBody()
            );
        } else {
            return new BookResponse(
                500,
                [
                    'Content-Type' => 'application/json'
                ],
                $this->getInvalids()
            );
        }
    }
}
