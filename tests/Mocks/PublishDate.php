<?php


namespace calderawp\InteropCore\Tests\Mocks;

use calderawp\InteropCore\AttributeHasValue;
use calderawp\InteropCore\AttributeIsDate;
use calderawp\InteropCore\IdentifyingAttribute;
use calderawp\InteropCore\ValidatingAttribute;
use Symfony\Component\Validator\Constraints\Date;

/**
 * Class PublishDate
 *
 * Attribute for testing that a date constraint allows a DateTime
 *
 * @package calderawp\InteropCore\Tests\Mocks
 */
class PublishDate extends ValidatingAttribute
{
    use IdentifyingAttribute;

    /** @inheritdoc */
    public function getConstraints()
    {
        return [
            new Date()
        ];
    }

    /** @inheritdoc */
    public function getPrimitive()
    {
        return \DateTime::class;
    }
}
