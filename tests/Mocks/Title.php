<?php


namespace calderawp\InteropCore\Tests\Mocks;

use calderawp\InteropCore\AttributeHasValue;
use calderawp\InteropCore\IdentifyingAttribute;
use calderawp\InteropCore\ValidatingAttribute;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

/**
 * Class Title
 *
 * Attribute for testing that a string constraint allows a string between 1 and 42 characters
 *
 * @package calderawp\InteropCore\Tests\Mocks
 */
class Title extends ValidatingAttribute
{
    use IdentifyingAttribute;

    /** @inheritdoc */
    public function getConstraints()
    {
        $length = new Length(
            [
                'min' => 1,
                'max' => 42
            ]
        );
        $length->min = 1;
        $length->max = 42;
        return [
            new NotBlank(),
            new NotNull(),
            $length
        ];
    }

    /** @inheritdoc */
    public function getPrimitive()
    {
        return 'string';
    }
}
