<?php


namespace calderawp\InteropCore\Tests;

use calderawp\InteropCore\InteroperableModel;
use calderawp\InteropCore\Tests\Mocks\BookModel;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\RequestException;

class ModelFromRequestTest extends TestCase
{

    /**
     * Test when all data
     *
     * @covers InteroperableModel::fromRequest()
     */
    public function testFromRequest()
    {
        $date = new \DateTime();
        $title = 'The Lord Of The Rings';
        $request = $this->postRequestFactory(
            [
                'PublishDate' => $date,
                'Title' => $title
            ]
        );

        $model = BookModel::fromRequest($request);
        $this->assertSame(
            $title,
            $model->getAttributeValue('Title')
        );

        $this->assertInstanceOf(
            \DateTime::class,
            $model->getAttributeValue('PublishDate')
        );

        $this->assertSame(
            $date->format('Y-m-d'),
            $model->getAttributeValue('PublishDate')->format('Y-m-d')
        );
    }

    /**
     * Test when a non-required field is missing that its default is provided in generated model
     *
     * @covers InteroperableModel::fromRequest()
     */
    public function testFromRequestDefault()
    {
        $date = new \DateTime();
        $request = $this->postRequestFactory(
            [
                'PublishDate' => $date->format('Y-m-d H:i:s'),
            ]
        );

        $model = BookModel::fromRequest($request);
        $this->assertSame(
            BookModel::TITLE_DEFUALT,
            $model->getAttributeValue('Title')
        );
    }

    /**
     * Test that an invalid attribute passed in request is ignored.
     * Also that it doesn't affect normal attributes and is not set in model.
     *
     * @covers InteroperableModel::fromRequest()
     * @covers InteroperableModel::setupAttributes()
     */
    public function testInvalidAttribute()
    {
        $date = new \DateTime();

        $requestBody = [
            'PublishDate' => $date->format('Y-m-d'),
            'HiRoy' => 'true'
        ];
        $request = $this->postRequestFactory($requestBody);

        $model = BookModel::fromRequest($request);
        $this->assertTrue($model->hasAttribute('PublishDate'));
        $this->assertEquals(
            $date->format('Y-m-d'),
            $model->getAttribute('PublishDate')->getValue()->format('Y-m-d')
        );
        $this->assertFalse($model->hasAttribute('HiRoy'));
        $this->assertFalse($model->hasAttribute($this->randomString(4)));
        $this->expectDefaultException();
        $model->getAttribute('HiRoy');
    }

    /**
     * @param $requestBody
     * @return Request
     */
    protected function postRequestFactory($requestBody): Request
    {
        $request = new Request(
            'POST',
            '/',
            [],
            \GuzzleHttp\json_encode($requestBody)
        );
        return $request;
    }
}
