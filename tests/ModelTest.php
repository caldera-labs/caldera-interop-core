<?php


namespace calderawp\InteropCore\Tests;

use calderawp\InteropCore\Exception;
use calderawp\InteropCore\HasValidatingAttributes;
use calderawp\InteropCore\InteroperableModel;
use calderawp\InteropCore\Tests\Mocks\BookModel;
use calderawp\InteropCore\Tests\Mocks\Title;
use calderawp\InteropCore\ValidatingAttribute;

class ModelTest extends TestCase
{





    /**
     * Test that not passing a required attribute raises an exception
     *
     * @covers InteroperableModel::setupAttributes()
     */
    public function testRequiredAttribute()
    {
        $this->expectDefaultException();
        $model = new BookModel(
            [
                'Title' => new Title()
            ]
        );
    }
}
