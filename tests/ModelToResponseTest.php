<?php


namespace calderawp\InteropCore\Tests;

use calderawp\InteropCore\InteroperableModel;
use calderawp\InteropCore\Tests\Mocks\BookModel;

class ModelToResponseTest extends TestCase
{
    /**
     * Test to response with valid data
     *
     * @covers InteroperableModel::toResponse()
     */
    public function testToResponseValid()
    {
        $this->assertTrue(true);
        $date = new \DateTime();
        $model = new BookModel(
            [
                'PublishDate' => $date,
                'Title' => 'Lord Of The Rings'
            ]
        );

        $this->assertTrue($model->isValid());
        $response = $model->toResponse();
        $body = json_decode(
            $response->getBody()->getContents()
        );

        $this->assertSame(
            $model->getAttributeValue('Title'),
            $body->Title
        );

        $this->assertEquals(200, $response->getStatusCode());
    }
}
