<?php


namespace calderawp\InteropCore\Tests\Models;

use calderawp\InteropCore\Tests\Mocks\BookModel;
use calderawp\InteropCore\Tests\Mocks\Title;
use calderawp\InteropCore\Tests\TestCase;

class AttributeDefaultsTest extends TestCase
{
    /**
     * @param $model
     */
    protected function a($model)
    {
        $this->assertSame(
            $model->getDefault('Title'),
            $model->getAttributeValue('Title')
        );
    }

    public function testHasDefault()
    {
        $model = new BookModel(
            [
                'PublishDate' => new \DateTime()
            ]
        );

        $this->assertTrue($model->hasDefault('Title'));
        $this->assertFalse($model->hasDefault('PANTS'));
    }

    public function testGetDefault()
    {
        $model = new BookModel(
            [
                'PublishDate' => new \DateTime()
            ]
        );

        $this->assertSame(
            BookModel::TITLE_DEFUALT,
            $model->getDefault('Title')
        );
    }

    /**
     * Test getting default value of attribute
     *
     * @covers InteroperableModel::getDefault()
     * @covers InteroperableModel::hasDefault()
     * @covers InteroperableModel::getAttributeValue()
     * @covers InteroperableModel::setupAttributes()
     */
    public function testGetAttributeValueWithDefault()
    {
        $model = new BookModel(
            [
                'PublishDate' => new \DateTime()
            ]
        );

        $this->assertSame(
            BookModel::TITLE_DEFUALT,
            $model->getAttributeValue('Title')
        );
    }
}
