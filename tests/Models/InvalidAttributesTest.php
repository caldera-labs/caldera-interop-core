<?php


namespace calderawp\InteropCore\Tests\Models;

use calderawp\InteropCore\Tests\Mocks\BookModel;
use calderawp\InteropCore\Tests\TestCase;

class InvalidAttributesTest extends TestCase
{

    /**
     * Ensure that passing a default value that is not valid will throw and exception
     *
     * Note: '1' is chosen as it should
     *
     * @covers InteroperableModel::setDefaultValues()
     * @covers ValidatingAttribute::setValue()
     */
    public function testInvalidDefault()
    {
        $value = new \DateTime();
        $this->expectDefaultException();
        $model = new BookModel(
            [
                'PublishDate' => new \stdClass()
            ]
        );
    }
}
