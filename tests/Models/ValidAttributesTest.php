<?php


namespace calderawp\InteropCore\Tests\Models;

use calderawp\InteropCore\Tests\Mocks\BookModel;
use calderawp\InteropCore\Tests\TestCase;
use calderawp\InteropCore\Tests\Mocks\PublishDate;

class ValidAttributesTest extends TestCase
{



    /**
     * Test getting attribute
     *
     * @covers BookModel::defineAttributes()
     * @covers InteroperableModel::defineAttributes()
     * @covers InteroperableModel::registerAttribute()
     * @covers InteroperableModel::setupAttributes()
     * @covers HasValidatingAttributes::getAttribute()
     */
    public function testGetAttribute()
    {
        $model = new BookModel(
            [
                'PublishDate' => new \DateTime()
            ]
        );

        $this->assertSame(
            'PublishDate',
            $model->getAttribute('PublishDate')->getIdentifier()
        );
    }

    /**
     * Test getting the value of an attribute
     *
     * @covers BookModel::defineAttributes()
     * @covers InteroperableModel::defineAttributes()
     * @covers InteroperableModel::registerAttribute()
     * @covers InteroperableModel::setupAttributes()
     * @covers InteroperableModel::getValidator()
     * @covers HasValidatingAttributes::getAttribute()
     * @covers InteroperableModel::getAttributeValue()
     */
    public function testGetAttributeValue()
    {
        $value = new \DateTime();
        $model = new BookModel(
            [
                'PublishDate' => $value
            ]
        );

        $this->assertSame(
            $value,
            $model->getAttribute('PublishDate')->getValue()
        );

        $this->assertSame(
            $value,
            $model->getAttributeValue('PublishDate')
        );


        $this->assertSame(
            $model->getAttribute('PublishDate')->getValue(),
            $model->getAttributeValue('PublishDate')
        );
    }
}
