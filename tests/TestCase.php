<?php


namespace calderawp\InteropCore\Tests;

use calderawp\InteropCore\Exception;
use PHPUnit\Framework\TestCase as PhpUnitTestCase;

abstract class TestCase extends PhpUnitTestCase
{
    /**
     * Assert that an exception of calderawp\InteropCore\Exception happens
     */
    protected function expectDefaultException()
    {
        $this->expectException(Exception::class);
    }

    /**
     * @param int $length
     * @return string
     */
    protected function randomString($length)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
